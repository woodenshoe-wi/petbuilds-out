
petbuilds-out
=============

These are pets compiled with the petbuilds from the 'generic' branch of
puppylinux-woof-CE/petbuilds.git.  This repo (petbuilds-out) is a git LFS repo,
if you want to clone it instead of downloading files from the web interface,
you will need to have git-lfs installed.

As far as I know my compiler isn't bugged (see
[Reflections on Trusting Trust](http://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf)
), but the paranoid may still want to re-compile for themselves using the
original petbuilds from
[https://github.com/puppylinux-woof-CE/petbuilds/](https://github.com/puppylinux-woof-CE/petbuilds/)
('generic' branch).

These may or may not have been tested
-------------------------------------

These pets are distributed in the hope that they will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Philosophy
----------

Puppy Linux was started by [Barry Kauler](http://bkhome.org/) using the
'benevolent dictator' model of organization.  Later when he moved on to other
interests, the Puppy Linux community continued on using the
['DoOcracy'](https://communitywiki.org/wiki/DoOcracy) model of organization (
see [Puppy Linux Team](http://puppylinux.com/team.html) ).

This means that trying to get people to 'do' something is like herding cats.
If you want me to do something that I think is a waste of time, I'm just not
going to do it, but _you_ are more than welcome to do it yourself. ;-)

The woof-CE system may still be a little rough around the edges... but even
with the handful of people working on the woof-CE system I think the Puppy
Linux community is larger than the average free software project...

From
[When Free Software Isn't (Practically) Superior](https://www.gnu.org/philosophy/when-free-software-isnt-practically-superior.html)
> Several academic studies of free software hosting sites SourceForge and
> Savannah have shown what many free software developers who have put a
> codebase online already know first-hand. The vast majority of free software
> projects are not particularly collaborative. The median number of
> contributors to a free software project on SourceForge? One. A lone
> developer. SourceForge projects at the ninety-fifth percentile by participant
> size have only five contributors. More than half of these free software
> projects—and even most projects that have made several successful releases
> and been downloaded frequently, are the work of a single developer with
> little outside help.

While the 'social coding' approach that GitHub advocates may help, some of us
are naturally somewhat asocial (not to be confused with anti-social) and
somewhat uncomfortable with 'social coding'.

I will try my best to accommodate any build methods that other people want to
use, but might choose not to use them myself.  I think striving for as much
flexibility as possible is key to making a doocratic project succeed.
